# Rapport de stage pour le Service des stages et du développement professionnel (SSDP)

## Processus pour obtenir un fichier au `format Word`

1. Éditer les fichiers `markdown` et `template LaTeX`
1. Exécuter `make` (`pandoc` produit un `PDF`)
1. Ouvrir le PDF avec `Word`
1. Effectuer des ajustements manuels
1. Enregistrer le fichier
