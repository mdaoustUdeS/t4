---
fontsize: 12pt
lang: fr
lieu: UNIVERSITÉ DE SHERBROOKE
title: RAPPORT DE STAGE
presented-to: Sonia Duquette, conseillère en développement professionnel
author: |
        | Matthieu Daoust, stagiaire
        | Ville de Sherbrooke
        | Infrastructure et sécurité
        | T4
date: 8 août 2022
geometry: margin=1in
header-includes:
    - \usepackage{times}
    - \usepackage{setspace}
    - \setstretch{1.5}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \renewcommand{\headrulewidth}{0pt}
    - \fancyhf{}
    - \lfoot{Matthieu Daoust}
    - \cfoot{\thepage}
    - \rfoot{Matthieu.Daoust@USherbrooke.ca}
---

# Partie A: Stage en sécurité infonuagique au sein d'une équipe d'infrastructure

## Mise en contexte

J'ai effectué mon stage en sécurité dans une équipe d'infrastructure pour la Ville de Sherbrooke, situé dans la ville de Sherbrooke, au Québec. La ville récemment commencée à effectuer une transition de certaines ressources physiques vers des ressources virtuelles dans un écosystème infonuagique. C'est dans ce contexte de transformation que j'ai aidé la Ville à améliorer sa posture de sécurité.

## Résumé du mandat

Lors de mon stage dans l'équipe d'infrastructure, j'ai eu plusieurs mandats afin d'augmenter la posture de sécurité de la Ville.

D'une part, j'ai eu à comprendre plusieurs services de l'environnement infonuagique d'Azure. J'ai aussi investigué les recommandations de Microsoft et je les ai appliqués lorsque nécessaire.

D'autre part, j'ai écrit et appliqué des politiques de gouvernance, notamment en lien avec l'authentification à multiples facteurs, afin que les recommandations de sécurité soient appliquées automatiquement.

## Conclusion sur mon expérience de stage

Après avoir travaillé à la Ville de Sherbrooke, je suis heureux d'avoir vécu une expérience positive d'apprentissage et de collaboration au sein de l'équipe d'infrastructure et de pérennité. Cela a confirmé mon intérêt à orienter mes prochains stages vers la sécurité.

# Partie B

## L'environnement

> _Décrivez votre milieu de travail, indiquez vos zones de confort et d’inconfort quant à ces conditions et expliquez pourquoi._

Il y a quelques années, les employés de l'organisation dans laquelle j'ai effectué mon stage avaient l'habitude de travailler dans un espace physique commun. Des restrictions gouvernementales ont fait en sorte que les membres de l'équipe ont été forcés à être complètement distribués à plusieurs endroits géographiques. Lorsque les restrictions ont été allégé, les membres de l'équipe ne sont pas tous retournés dans le même espace physique. En effet, ils ont continué à travailler de façon distribuée. J'ai apprécié l'expérience que j'ai vécue dans cette équipe distribuée. J'ai aussi apprécié d'utiliser les espaces de travail à distance qui étaient disponibles au bureau. Cela permettait de travailler à distance dans plusieurs environnements différents.

La majorité des équipes du service dans lequel j'étais impliqué utilisait des concepts de la méthodologie Scrum, dont des courtes réunions journalières en début de journée. Ces rencontres ont permis de faire en sorte que l'équipe communique fréquemment sur l'état d'avancement global des projets et d'accompagner ceux qui ont besoin d'aide.

## Les connaissances

> _Que savez-vous maintenant, qui vous aidera pour vos prochains stages ou votre emploi? Pourquoi?_

Durant ce stage j'ai acquis de nouvelles connaissances qui me seront utiles tout au long de ma vie.

D'une part, j'ai appris de nouveaux concepts en lien avec la sécurité. Lorsque j'ai rencontré des difficultés, j'ai cherché l'information afin de pouvoir avancer. Ces concepts importants me seront utiles à l'avenir puisque je serai en mesure de les appliquer dans différents contextes.

D'autre part, j'ai eu à me familiariser avec plusieurs services de la plateforme infonuagique Azure de Microsoft. En effet, en investiguant et en appliquant les recommandations de _Microsoft Defender pour le cloud_, j'ai pu travailler avec plusieurs ressources d'Azure. Ces connaissances me seront utiles à l'avenir puisque je serai en mesure de mieux comprendre les plateformes infonuagiques.

## Le professionnalisme

> _Avez-vous fait des apprentissages particuliers en lien avec certaines règles ou normes et quel comportement avez-vous adopté?_

Lorsqu'on a accès à plusieurs systèmes sensibles, il est nécessaire de faire preuve de prudence afin d'éviter que nos actions aient des répercussions négatives. En effet, une erreur de configuration dans Azure a le potentiel d'avoir un impact sur une grande quantité d'utilisateurs ou d'avoir des conséquences désastreuses.

Lors de mon stage à la Ville de Sherbrooke, j'ai eu accès à plusieurs ressources sensibles et une mauvaise manipulation aurait pu causer des situations catastrophiques ou avoir un impact sur plusieurs utilisateurs. Afin de réduire le risque, j'ai pris le temps de discuter avec mes collègues avant de faire des actions à fort niveau de risque, tel que la suppression de ressources, activation de services à coût élevé, modification de droits d'accès, etc. Lorsque c'était possible, j'ai aussi surveillé les impacts potentiels avant d'appliquer des changements, tels qu'aux politiques d'authentifications à plusieurs facteurs ou aux politiques de gouvernance infonuagiques. J'ai apprécié cette façon de fonctionner, puisque cela m'a permis de détecter plusieurs situations problématiques avant que cela ait un impact réel.

## La connaissance de soi

> _Quels intérêts avez-vous découverts ou confirmés lors de cette expérience?_

Durant cette expérience de stage, j'ai eu la chance d'apprendre beaucoup. En effet, j'ai eu à travailler avec plusieurs outils dont je n'avais jamais eu l'occasion d'explorer. J'ai vraiment apprécié de pouvoir découvrir de nouveaux outils et apprendre beaucoup de concepts.

Aussi, je suis reconnaissant du temps que j'ai passé avec des individus passionnés de leur travail. Ce fut fort agréable et enrichissant de discuter avec mes collègues.

Enfin, cette expérience m'a confirmé mon intérêt pour effectuer mes prochains stages dans le domaine de la sécurité informatique.
